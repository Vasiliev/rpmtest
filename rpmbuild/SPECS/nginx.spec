Name        : nginx
Epoch       : 1
Version     : 1.14.0
Release     : 1.el7_4.ngx
BuildArch   : x86_64
Group       : System Environment/Daemons
License     : 2-clause BSD-like license
Vendor      : Nginx, Inc.
URL         : http://nginx.org/
Summary     : High performance web server
Source      : nginx-1.14.0-1.el7_4.ngx.x86_64.tar.gz

%description
nginx [engine x] is an HTTP and reverse proxy server, as well as
a mail proxy server.

%prep
%setup -c $NAME-$VERSION-$RELEASE.$BUILDARCH

%build

%install
cp -rfa * %{buildroot}

%files
# /*
%config(noreplace) %attr(0644, root, root) "/etc/logrotate.d/nginx"
%dir %attr(0755, root, root) "/etc/nginx"
%dir %attr(0755, root, root) "/etc/nginx/conf.d"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/conf.d/default.conf"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/fastcgi_params"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/koi-utf"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/koi-win"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/mime.types"
%attr(0777, root, root) "/etc/nginx/modules"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/nginx.conf"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/scgi_params"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/uwsgi_params"
%config(noreplace) %attr(0644, root, root) "/etc/nginx/win-utf"
%config(noreplace) %attr(0644, root, root) "/etc/sysconfig/nginx"
%config(noreplace) %attr(0644, root, root) "/etc/sysconfig/nginx-debug"
%attr(0644, root, root) "/usr/lib/systemd/system/nginx-debug.service"
%attr(0644, root, root) "/usr/lib/systemd/system/nginx.service"
%dir %attr(0755, root, root) "/usr/lib64/nginx"
# %dir %attr(0755, root, root) "/usr/lib64/nginx/modules"
%dir %attr(0755, root, root) "/usr/libexec/initscripts/legacy-actions/nginx"
%attr(0755, root, root) "/usr/libexec/initscripts/legacy-actions/nginx/check-reload"
%attr(0755, root, root) "/usr/libexec/initscripts/legacy-actions/nginx/upgrade"
%attr(0755, root, root) "/usr/sbin/nginx"
%attr(0755, root, root) "/usr/sbin/nginx-debug"
%dir %attr(0755, root, root) "/usr/share/doc/nginx-1.14.0"
%doc %attr(0644, root, root) "/usr/share/doc/nginx-1.14.0/COPYRIGHT"
%doc %attr(0644, root, root) "/usr/share/man/man8/nginx.8.gz"
%dir %attr(0755, root, root) "/usr/share/nginx"
%dir %attr(0755, root, root) "/usr/share/nginx/html"
%attr(0644, root, root) "/usr/share/nginx/html/50x.html"
%attr(0644, root, root) "/usr/share/nginx/html/index.html"
%dir %attr(0755, root, root) "/var/cache/nginx"
%dir %attr(0755, root, root) "/var/log/nginx"
%dir %attr(0775, root, root) "/data/www"
%doc %attr(0644, root, root) "/data/www/index.html"
%doc %attr(0644, root, root) "/var/log/nginx/access.log"
%doc %attr(0644, root, root) "/var/log/nginx/error.log"

%changelog


%description
nginx [engine x] is an HTTP and reverse proxy server, as well as
a mail proxy server.

%pre
# Add the "nginx" user
getent group nginx >/dev/null || groupadd -r nginx
getent passwd nginx >/dev/null || \
    useradd -r -g nginx -s /sbin/nologin \
    -d /var/cache/nginx -c "nginx user"  nginx

%post
# Register the nginx service
if [ $1 -eq 1 ]; then
    /usr/bin/systemctl preset nginx.service >/dev/null 2>&1 ||:
    /usr/bin/systemctl preset nginx-debug.service >/dev/null 2>&1 ||:
    # print site info
    cat <<BANNER
----------------------------------------------------------------------

Thanks for using nginx!

Please find the official documentation for nginx here:
* http://nginx.org/en/docs/

Please subscribe to nginx-announce mailing list to get
the most important news about nginx:
* http://nginx.org/en/support.html

Commercial subscriptions for nginx are available on:
* http://nginx.com/products/

----------------------------------------------------------------------
BANNER

    # Touch and set permisions on default log files on installation

    if [ -d /var/log/nginx ]; then
        if [ ! -e /var/log/nginx/access.log ]; then
            touch /var/log/nginx/access.log
            /usr/bin/chmod 640 /var/log/nginx/access.log
            /usr/bin/chown nginx:adm /var/log/nginx/access.log
        fi

        if [ ! -e /var/log/nginx/error.log ]; then
            touch /var/log/nginx/error.log
            /usr/bin/chmod 640 /var/log/nginx/error.log
            /usr/bin/chown nginx:adm /var/log/nginx/error.log
        fi
    fi
    if [ -d /data/www ]; then
        # /usr/bin/chmod -R 775 /data/www
        /usr/bin/chown -R nginx:adm /data/www
        fi
fi

%preun
if [ $1 -eq 0 ]; then
    /usr/bin/systemctl --no-reload disable nginx.service >/dev/null 2>&1 ||:
    /usr/bin/systemctl stop nginx.service >/dev/null 2>&1 ||:
fi
%postun
/usr/bin/systemctl daemon-reload >/dev/null 2>&1 ||:
if [ $1 -ge 1 ]; then
    /sbin/service nginx status  >/dev/null 2>&1 || exit 0
    /sbin/service nginx upgrade >/dev/null 2>&1 || echo \
        "Binary upgrade failed, please check nginx's error.log"
fi
