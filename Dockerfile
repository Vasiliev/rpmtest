FROM chentex/docker-nginx-centos:latest-centos7 as build
 
ENV nginxversion="1.14.0-1" \
    os="centos" \
    osversion="7" \
    elversion="7_4"

RUN yum -y install epel-release &&\
    yum repolist &&\
    yum install -y wget openssl sed &&\
    yum install -y rpm-build&&\
    yum -y autoremove &&\
    yum clean all

COPY build/ /root/rpmbuild/

RUN rpm -ql nginx-1.14.0-1.el7_4.ngx.x86_64 | sed -e '$a\/data\/www\/index.html'| sed '/\/$/d' | xargs tar -zcvf ~/rpmbuild/SOURCES/nginx-1.14.0-1.tar.gz

RUN rpmbuild -bb /root/rpmbuild/SPECS/nginx.spec


FROM centos:centos7

WORKDIR /app
COPY --from=build /root/rpmbuild/RPMS/x86_64/nginx-1.14.0-1.el7_4.ngx.x86_64.rpm .

RUN rpm -i nginx-1.14.0-1.el7_4.ngx.x86_64.rpm

VOLUME [ "/data/www" ]
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]